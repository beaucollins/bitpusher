package com.redimastudio {
  
  import flash.display.*;
  import flash.net.URLRequest;
  import flash.events.MouseEvent;
  import flash.external.ExternalInterface;
  import flash.display.Stage;
  import flash.display.StageAlign;
  import flash.display.StageScaleMode;
  import flash.net.FileReferenceList;
  import flash.events.Event;
  import flash.net.FileReference;
  import flash.events.IOErrorEvent;
  import flash.events.HTTPStatusEvent;
  import flash.events.ProgressEvent;
  import flash.events.DataEvent;
  import flash.events.SecurityErrorEvent;
  
  /* 
  
  Main class for the document, displays an upload button, shows the
  file selection dialog, then provides some hooks/callbacks to let HTML/JS do
  the rest of the UI
  
  Configuration:
    - Upload button url
  
  */
  public class BitPusher extends MovieClip {
    
    private var upload_button_url:String;
    public var upload_button:MovieClip;
    
    private var file_browser:FileReferenceList;
    private var fileList:FileList;
    
    public function BitPusher(){
      
      fileList = new FileList();
      
      stage.align = StageAlign.TOP_LEFT;
      stage.scaleMode = StageScaleMode.NO_SCALE;
      
      upload_button = new MovieClip;
      addChild(upload_button);
      upload_button.buttonMode = true;
      upload_button.useHandCursor = true;
      
      // Quick and dirty download button
      var loader:Loader = new Loader();
      loader.load(new URLRequest(loadParam('upload_button_url', 'images/upload_button.png')));
      upload_button.addChild(loader);      
      upload_button.addEventListener(MouseEvent.CLICK, onUploadClicked);
      
      if(ExternalInterface.available){
        ExternalInterface.addCallback('upload', uploadFile);
        ExternalInterface.addCallback('cancel', cancelFileUpload);
      }
    }
    
    /* Javascript Interface Methods  */
    
    /*
      Removes the file from the upload list
    */
    public function removeFile( index:int ) : void {
      var file:IndexedFileReference = fileList.getFile(index);
      dispatch('file_removed', file.memo);
    }
    
    public function uploadFile( index:int, url:String, fieldName:String, postData:String ) : void {
      var file:IndexedFileReference = fileList.getFile(index);

      addFileListeners(file);

      file.startUpload(url, fieldName, postData);
      
    }
    
    public function cancelFileUpload( index:int ) : void {
      var file:IndexedFileReference = fileList.getFile(index);
      file.canceled = true;
      dispatch('file_upload_canceled', file.memo);
    }
    
    
    private function addFileListeners(file:IndexedFileReference){
      file.addEventListener(Event.OPEN, onFileOpen); // open
      file.addEventListener(ProgressEvent.PROGRESS, onFileUploadProgress); // progress
      file.addEventListener(Event.COMPLETE, onFileUploadComplete); // complete
      file.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileUploadCompleteData);
      file.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onFileUploadSecurityError);
      file.addEventListener(HTTPStatusEvent.HTTP_STATUS, onFileStatus); // httpStatus
      file.addEventListener('httpResponseStatus', onFileUploadHTTPResponse);
      file.addEventListener(IOErrorEvent.IO_ERROR, onFileIOError); // ioError
    }
    
    /* File uploading events */
    
    public function onFileIOError(event:IOErrorEvent){
      dispatch('file_upload_error', { error: event.text, id: event.target.index });
    }
    
    public function onFileStatus(event:HTTPStatusEvent){
      var memo = event.target.memo;
      memo['status'] = event.status;
      dispatch('file_upload_status', memo);
    }
    
    public function onFileUploadComplete(event:Event){
      dispatch('file_upload_complete', event.target.memo );
    }
    
    public function onFileUploadCompleteData(event:DataEvent){
      var memo = event.target.memo;
      memo['data'] = event.data;
      dispatch('file_upload_complete_data', memo );
    };
    
    public function onFileUploadProgress(event:ProgressEvent){
      dispatch('file_upload_progress', event.target.memo);
    }
    
    public function onFileOpen(event:Event){
      dispatch('file_upload_started', event.target.memo);
    }
        
    public function onFileUploadHTTPResponse(event:HTTPStatusEvent){
      dispatch('file_upload_http_response', event.target.memo);
    }
    
    public function onFileUploadSecurityError(event:SecurityErrorEvent){
      dispatch('file_upload_security_error', event.target.memo);
    }
    
    
    /*
      Sets up the file reference and call backs and such
    */
    
    public function registerFileReference(file_ref:Object, index:int, array:Array){
      var file:IndexedFileReference = fileList.add(FileReference(file_ref));
      
      dispatch('file_added', file.memo);
      
    }
    
    
    public function onUploadClicked( event:MouseEvent ) : void {
      file_browser = new FileReferenceList();
      file_browser.addEventListener(Event.SELECT, onSelectFiles);
      file_browser.addEventListener(Event.CANCEL, onCancelSelection);
      file_browser.browse();
    }
    
    public function onSelectFiles( event:Event ) : void {
      file_browser.fileList.forEach(registerFileReference);
    }
    
    public function onCancelSelection( event:Event ) : void{
      
    }
    
    private function loadParam(key:String, default_value:Object) {
      var param_object:Object = loaderInfo.parameters;
      if(param_object[key]){
        return param_object[key]
      }else{
        return default_value;
      }
      
    }
    
    private function dispatch(event_name, memo){
      
      if(ExternalInterface.available){
        ExternalInterface.call('BitPusher.dispatch', event_name, memo);        
      }else{
        trace([event_name, memo]);
      }
    }
    
    private function exCall(to_call, arg){
      trace("calling via external interface: " + ExternalInterface.available);
      if(ExternalInterface.available){
        ExternalInterface.call(to_call, arg);        
      }else{
        trace([to_call, arg]);
      }
    }
    
  }
    
  
}