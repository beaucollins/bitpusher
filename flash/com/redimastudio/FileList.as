package com.redimastudio {
  
  import flash.net.FileReference;
  
  class FileList {
    
    private var list:Array;
    private var total_bytes:int;
    
    public function FileList(){
      list = new Array();
      total_bytes = 0;
    }
    
    public function add(file:FileReference) : IndexedFileReference{
      var indexed_file:IndexedFileReference = new IndexedFileReference(file, nextIndex());
      list.push(indexed_file);
      total_bytes += file.size;
      return indexed_file;
    }
    
    public function getFile(index:int) : IndexedFileReference {
      var file:IndexedFileReference = IndexedFileReference(list[index]);
      return file;
    }
    
    public function nextIndex() : int {
      return list.length;
    }
    
    public function get totalBytes() : int {
      return total_bytes;
    }
    
  }
  
}
