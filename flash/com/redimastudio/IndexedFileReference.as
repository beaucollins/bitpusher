package com.redimastudio {
  
  import flash.net.FileReference;
  import flash.net.URLRequest;
  import flash.net.URLRequestMethod;
  import flash.net.URLVariables;
  import flash.events.IEventDispatcher;
  import flash.events.EventDispatcher;
  import flash.events.Event;
  import flash.events.IOErrorEvent;
  import flash.external.ExternalInterface;
  import flash.events.HTTPStatusEvent;
  import flash.events.DataEvent;
  import flash.events.SecurityErrorEvent;
  import flash.events.ProgressEvent;
  
  class IndexedFileReference implements IEventDispatcher {
    
    private var dispatcher:EventDispatcher;
    
    private var internal_reference:FileReference;
    private var _index:int;
    private var _uploaded_bytes:Number;
    private var _canceled:Boolean;
    
    public function IndexedFileReference (file_reference:FileReference, file_index:int){
      dispatcher = new EventDispatcher(this);
      internal_reference = file_reference;
      _uploaded_bytes = 0;
      _index = file_index;
      _canceled = false;
    }
    
    public function get fileReference():FileReference {
      return internal_reference;
    }
    
    public function get index():int {
      return _index;
    }
    
    public function get name() : String {
      return internal_reference.name;
    }
    
    public function get size() : Number {
      return internal_reference.size;
    }
    
    public function get uploadedBytes() : Number {
      return _uploaded_bytes;
    }
    
    public function get memo() : Object {
      return {
        'id' : this.index,
        'name' : this.name,
        'size' : this.size,
        'uploaded_bytes' : this.uploadedBytes
      }
    }
    
    public function get canceled() : Boolean {
      return _canceled;
    }
    
    public function set canceled( status:Boolean ) : void {
      if(!canceled){
        _canceled = true;
        cancel();
      }
    }
    
    public function startUpload(url:String, field_name:String, postData:String){
      var req:URLRequest = new URLRequest(url);
      req.method = URLRequestMethod.POST;
      req.data = postData;
      
      addUploadListeners();
      
      internal_reference.upload(req, field_name);
    }
    
    public function eventProxy(event:Event){
      if(event is ProgressEvent){
        var progress_event = ProgressEvent(event);
        _uploaded_bytes = progress_event.bytesLoaded;
      }
      var clone_event = event.clone();
      dispatchEvent(clone_event);
    }
    
    private function addUploadListeners(){
      var i:FileReference = internal_reference;
      ([Event.OPEN,
      ProgressEvent.PROGRESS,
      Event.COMPLETE,
      DataEvent.UPLOAD_COMPLETE_DATA,
      HTTPStatusEvent.HTTP_STATUS,
      'httpResponseStatus',
      SecurityErrorEvent.SECURITY_ERROR,
      IOErrorEvent.IO_ERROR]).forEach(function(event_name:Object, index:int, array:Array){
        i.addEventListener(String(event_name), eventProxy);
      })
      
    }
    
    private function removeUploadListeners(){
      internal_reference.removeEventListener(IOErrorEvent.IO_ERROR, eventProxy);
      internal_reference.removeEventListener(HTTPStatusEvent.HTTP_STATUS, eventProxy);
    }
    
    public function cancel(){
      internal_reference.cancel();
    }
    
    /* listen to the actual file */
    
    
        
    /* IEventDispatcher methods */
    
    public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void{
      dispatcher.addEventListener(type, listener, useCapture, priority);
    }
           
    public function dispatchEvent(evt:Event):Boolean{
      return dispatcher.dispatchEvent(evt);
    }
    
    public function hasEventListener(type:String):Boolean{
      return dispatcher.hasEventListener(type);
    }
    
    public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void{
      dispatcher.removeEventListener(type, listener, useCapture);
    }
                   
    public function willTrigger(type:String):Boolean {
      return dispatcher.willTrigger(type);
    }
    
  }
  
}