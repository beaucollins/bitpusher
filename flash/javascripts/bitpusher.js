var BitPusher = {
  init:function(element_id){
    var element = $(element_id);
    swfobject.embedSWF('bitpusher.swf', element_id, element.getWidth(), element.getHeight(), '9.0', '', {}, {wmode:'transparent'});
    this.element = $(element_id);
  },
  dispatch:function(event_name, memo){
    event = this.element.fire('bitpusher:' + event_name, memo);
  },
  observe:function(event_name, callback){
    this.element.observe('bitpusher:' + event_name, callback);
  },
  startUpload:function(id, url, field_name, post_data){
    this.element.upload(id, url, field_name, post_data);
  },
  cancelUpload:function(id){
    this.element.cancel(id);
  }
}
